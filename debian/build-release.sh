#!/bin/bash

mkdir -p EmailCollectorApp/var/lib/EmailCollectorApp

cd ..
./gradlew clean build

G=$(grep "^version =" build.gradle | sed -e "s/version = //g" | sed -e "s/'//g")

echo "found version $G"

rm -f debian/EmailCollectorApp/var/lib/EmailCollectorApp/*.jar
cp -v build/libs/EmailCollectorApp-$G.jar debian/EmailCollectorApp/var/lib/EmailCollectorApp

cd debian

sed -i EmailCollectorApp/DEBIAN/control -e "s/VERSION/$G/g"
sed -i EmailCollectorApp/usr/bin/EmailCollectorApp -e "s/VERSION/$G/g"

V=$(grep "Version" EmailCollectorApp/DEBIAN/control | sed -e "s/Version: //g")

fakeroot dpkg -b EmailCollectorApp EmailCollectorApp_$V.deb

package de.ship.pia.index;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.ship.pia.common.ValidationMessage;
import de.ship.pia.emailcollector.jooq.tables.records.TPersonRecord;
import de.ship.pia.index.impl.IndexService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(IndexController.class)
class IndexControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IndexService indexService;

    @Test
    void getIndexPageTest() throws Exception {
        this.mvc.perform(get("/")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("emailSettings"))
                .andExpect(view().name(IndexController.INDEX_PAGE));
    }

    @Test
    void postIndexPageSuccessTest() throws Exception {
        this.mvc.perform(post("/")
                        .param("id", "pseudo-id")
                        .param("password", "password")
                        .param("email", "email@address.outhere")
                        .param("saveEmailForPia", "true")
                        .param("saveEmailForShip", "true")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute(ValidationMessage.SUCCESS, ValidationMessage.SUCCESS_MESSAGE))
                .andExpect(view().name(IndexController.INDEX_PAGE));

        verify(indexService, times(1)).submit(any(TPersonRecord.class), eq("password"),
                anyString(), anyString(), anyString());
    }

    @Test
    void postIndexPageWithBindingErrorsTest() throws Exception {
        this.mvc.perform(post("/")
                        .param("id", "pseudo-id")
                        .param("password", "password")
                        .param("email", "email@address.outhere")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(model().hasErrors())
                .andExpect(model().attribute(ValidationMessage.ERROR, ValidationMessage.FORM_ERROR))
                .andExpect(view().name(IndexController.INDEX_PAGE));

        verify(indexService, never()).submit(any(TPersonRecord.class), anyString(), anyString(), anyString(), anyString());
    }

    @Test
    void postIndexPageWithLoginErrorTest() throws Exception {
        when(indexService.submit(any(TPersonRecord.class), eq("wrong-password"), anyString(), anyString(), anyString()))
                .thenReturn(PiaErrorMessage.LOGIN_FAILED.get());

        this.mvc.perform(post("/")
                        .param("id", "pseudo-id")
                        .param("password", "wrong-password")
                        .param("email", "email@address.outhere")
                        .param("saveEmailForPia", "true")
                        .param("saveEmailForShip", "false")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(model().attribute(ValidationMessage.ERROR, ValidationMessage.LOGIN_ERROR_MESSAGE))
                .andExpect(view().name(IndexController.INDEX_PAGE));
    }

    @Test
    void postIndexPageWithTooManyLoginAttempsTest() throws Exception {
        when(indexService.submit(any(TPersonRecord.class), eq("wrong-password"), anyString(), anyString(), anyString()))
                .thenReturn(PiaErrorMessage.FAILED_LOGIN_ATTEMPTS.get());

        this.mvc.perform(post("/")
                        .param("id", "pseudo-id")
                        .param("password", "wrong-password")
                        .param("email", "email@address.outhere")
                        .param("saveEmailForPia", "true")
                        .param("saveEmailForShip", "false")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(model().attribute(ValidationMessage.ERROR, ValidationMessage.LOGIN_FAILURE))
                .andExpect(view().name(IndexController.INDEX_PAGE));
    }

    @Test
    void postIndexPageWrongStudyTest() throws Exception {
        when(indexService.submit(any(TPersonRecord.class), eq("password"), anyString(), anyString(), anyString()))
                .thenReturn(PiaErrorMessage.WRONG_STUDY.get());

        this.mvc.perform(post("/")
                        .param("id", "pseudo-id")
                        .param("password", "password")
                        .param("email", "email@address.outhere")
                        .param("saveEmailForPia", "true")
                        .param("saveEmailForShip", "false")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(model().attribute(ValidationMessage.ERROR, ValidationMessage.LOGIN_WRONG_STUDY))
                .andExpect(view().name(IndexController.INDEX_PAGE));
    }

    @Test
    void postIndexPageWithUnknownErrorTest() throws Exception {
        String errorText = "Unbekannter Fehler aufgetreten!";
        when(indexService.submit(any(TPersonRecord.class), eq("password"), anyString(), anyString(), anyString()))
                .thenReturn(errorText);

        this.mvc.perform(post("/")
                        .param("id", "pseudo-id")
                        .param("password", "password")
                        .param("email", "email@address.outhere")
                        .param("saveEmailForPia", "true")
                        .param("saveEmailForShip", "false")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(model().attribute(ValidationMessage.ERROR_STRING, errorText))
                .andExpect(view().name(IndexController.INDEX_PAGE));
    }

}
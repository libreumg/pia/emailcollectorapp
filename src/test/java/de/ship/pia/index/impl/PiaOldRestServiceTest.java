package de.ship.pia.index.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;

import de.ship.pia.index.impl.piarest.JsonError;

/**
 * 
 * @author henkej
 *
 */
@Disabled("Integration test that are run manually")
public class PiaOldRestServiceTest {

	@Test
	public void testLogin() throws FileNotFoundException, IOException {
		File configFile = new File("/home/henkej/.piadev");
		if (configFile.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(configFile));
			String baseUrl = props.getProperty("baseurl");
			String username = props.getProperty("username");
			String password = props.getProperty("password");

			PiaOldRestService service = new PiaOldRestService();
			String token = service.login(baseUrl, null, username, password, "pia-admin-web-app-client");
			assertNotNull(token);
			System.out.println(token);
		} else {
			fail(String.format("%s does not exist...", configFile.getAbsolutePath()));
		}
	}

	@Test
	public void testIsKeycloakAware() throws FileNotFoundException, IOException {
		File configFile = new File("src/main/resources/application.properties");
		if (configFile.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(configFile));
			String baseUrl = props.getProperty("pia.baseurl");

			assertFalse(new PiaOldRestService().isKeycloakAware(baseUrl));
		} else {
			fail(String.format("%s does not exist...", configFile.getAbsolutePath()));
		}
	}

	@Test
	public void testAddEmail() throws FileNotFoundException, IOException {
		File configFile = new File("/home/henkej/.piadev");
		if (configFile.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(configFile));
			String baseUrl = props.getProperty("baseurl");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			PiaOldRestService service = new PiaOldRestService();
			String token = service.login(baseUrl, null, username, password, "pia-admin-web-app-client");
			assertNotNull(token);
			String pseudonym = props.getProperty("old.username");
			String userPassword = props.getProperty("old.password");
			String eMail = props.getProperty("old.email");

			try {
				service.login(baseUrl, null, pseudonym, userPassword, "pia-proband-web-app-client");
				service.addEmail(baseUrl, token, pseudonym, eMail);
			} catch (HttpClientErrorException e) {
				String jsonPart = e.getResponseBodyAsString();
				JsonError errorBean = new Gson().fromJson(jsonPart, JsonError.class);
				throw new IOException(errorBean.getMessage());
			}
		} else {
			fail(String.format("%s does not exist...", configFile.getAbsolutePath()));
		}
	}

}

package de.ship.pia.index.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import de.ship.pia.emailcollector.jooq.tables.records.TPersonRecord;
import de.ship.pia.index.PiaErrorMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
class IndexServiceTest {

    private static final String BASE_URL = "base-url";
    private static final String PSEUDO_ID = "pseudo-id";
    private static final String PASSWORD = "password";
    private static final String PM_USERNAME = "probandenmanager";
    private static final String PM_PASSWORD = "probandenmanager-passwort";


    @Mock
    private DatabaseRepository databaseRepository;

    @Mock
    private PiaOldRestService piaOldRestRepository;

    @Mock
    private PiaKeycloakRestService piaKeycloakRestRepository;

    @InjectMocks
    private IndexService indexService;

    @Test
    void submitThrowsNullPointerException() {
        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(null);

        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> {
            indexService.submit(new TPersonRecord(), PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);
        }, "NullPointerException was expected");

        assertEquals("could not determine version of PIA from the version page", thrown.getMessage());
    }

    @Test
    void submitWithOldInterfaceSuccessful() {
        TPersonRecord tPersonRecord = new TPersonRecord();
        tPersonRecord.setPseudoId(PSEUDO_ID);

        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(false);
        String errorMessage = indexService.submit(tPersonRecord, PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);

        assertNull(errorMessage);
    }

    @Test
    void submitUsesOldInterface() {
        TPersonRecord tPersonRecord = new TPersonRecord();
        tPersonRecord.setPseudoId(PSEUDO_ID);

        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(false);
        indexService.submit(tPersonRecord, PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);

        verify(piaKeycloakRestRepository, never())
                .login(anyString(), anyString(), anyString(), anyString(), anyString());
        verify(piaKeycloakRestRepository, never())
                .login(anyString(), anyString(), anyString(), anyString(), anyString());

        verify(piaOldRestRepository, times(1))
                .login(BASE_URL, "pia-proband-realm", PSEUDO_ID, PASSWORD, "pia-proband-web-app-client");
        verify(piaOldRestRepository, times(1))
                .login(BASE_URL, "pia-admin-realm", PM_USERNAME, PM_PASSWORD, "pia-admin-web-app-client");
    }

    @Test
    void submitWithKeykloakInterfaceSuccessful() {
        TPersonRecord tPersonRecord = new TPersonRecord();
        tPersonRecord.setPseudoId(PSEUDO_ID);

        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(true);
        String errorMessage = indexService.submit(tPersonRecord, PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);

        assertNull(errorMessage);
    }

    @Test
    void submitUsesKeykloakInterface() {
        TPersonRecord tPersonRecord = new TPersonRecord();
        tPersonRecord.setPseudoId(PSEUDO_ID);

        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(true);
        indexService.submit(tPersonRecord, PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);

        verifyNoInteractions(piaOldRestRepository);
        verify(piaKeycloakRestRepository, times(1))
                .login(BASE_URL, "pia-proband-realm", PSEUDO_ID, PASSWORD, "pia-proband-web-app-client");
        verify(piaKeycloakRestRepository, times(1))
                .login(BASE_URL, "pia-admin-realm", PM_USERNAME, PM_PASSWORD, "pia-admin-web-app-client");
    }

    @Test
    void userLoginFailure() {
        TPersonRecord tPersonRecord = new TPersonRecord();
        tPersonRecord.setPseudoId(PSEUDO_ID);

        String responseBody = "{'statusCode': 403, 'error': 'Login Failure', 'message': '" + PiaErrorMessage.LOGIN_FAILED.get() + "'}";
        HttpClientErrorException exception = new HttpClientErrorException(HttpStatus.FORBIDDEN, "status-text",
                responseBody.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);

        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(true);
        when(piaKeycloakRestRepository.login(BASE_URL, "pia-proband-realm", PSEUDO_ID, PASSWORD, "pia-proband-web-app-client"))
                .thenThrow(exception);

        String errorMessage = indexService.submit(tPersonRecord, PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);

        verifyNoInteractions(databaseRepository);
        assertEquals(errorMessage, PiaErrorMessage.LOGIN_FAILED.get());
    }

    @Test
    void probandManagerLoginFailure() {
        String email = "email@adress.outhere";
        LocalDateTime consentPia = LocalDateTime.now();
        LocalDateTime consentShip = LocalDateTime.now();

        TPersonRecord tPersonRecord = new TPersonRecord();
        tPersonRecord.setPseudoId(PSEUDO_ID);
        tPersonRecord.setEmail(email);
        tPersonRecord.setConsentPia(consentPia);
        tPersonRecord.setConsentShip(consentShip);

        String responseBody = "{'statusCode': 403, 'error': 'Login Failure', 'message': '" + PiaErrorMessage.LOGIN_FAILED.get() + "'}";
        HttpClientErrorException exception = new HttpClientErrorException(HttpStatus.FORBIDDEN, "status-text",
                responseBody.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);

        when(piaKeycloakRestRepository.isKeycloakAware(BASE_URL)).thenReturn(true);
        when(piaKeycloakRestRepository.login(BASE_URL, "pia-proband-realm", PSEUDO_ID, PASSWORD, "pia-proband-web-app-client"))
                .thenReturn("user-token");
        when(piaKeycloakRestRepository.login(BASE_URL, "pia-admin-realm", PM_USERNAME, PM_PASSWORD, "pia-admin-web-app-client"))
                .thenThrow(exception);

        String errorMessage = indexService.submit(tPersonRecord, PASSWORD, BASE_URL, PM_USERNAME, PM_PASSWORD);

        verify(databaseRepository).addEmail(PSEUDO_ID, email, consentPia, consentShip);
        verify(piaKeycloakRestRepository, never()).addEmail(any(), any(), any(), any());

        assertEquals(errorMessage, PiaErrorMessage.LOGIN_FAILED.get());
    }
}
package de.ship.pia.index.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;

import de.ship.pia.index.impl.piarest.JsonError;

/**
 * 
 * @author henkej
 *
 */
@Disabled("Integration tests that are run manually")
public class PiaKeycloakRestServiceTest {


	@Test
	public void testLogin() throws FileNotFoundException, IOException {
		File configFile = new File("/home/henkej/.piadev");
		if (configFile.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(configFile));
			String baseUrl = props.getProperty("baseurlKC");
			String username = props.getProperty("usernameKC");
			String password = props.getProperty("passwordKC");

			PiaKeycloakRestService service = new PiaKeycloakRestService();
			String token = service.login(baseUrl, "pia-admin-realm", username, password, "pia-admin-web-app-client");
			assertNotNull(token);
			System.out.println(token);
		} else {
			fail(String.format("%s does not exist...", configFile.getAbsolutePath()));
		}
	}

	@Test
	public void testIsKeycloakAware() throws FileNotFoundException, IOException {
		File configFile = new File("src/main/resources/application.properties");
		if (configFile.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(configFile));
			String baseUrl = props.getProperty("pia.baseurl");

			assertFalse(new PiaKeycloakRestService().isKeycloakAware(baseUrl));
		} else {
			fail(String.format("%s does not exist...", configFile.getAbsolutePath()));
		}
	}

	@Test
	public void testAddEmail() throws FileNotFoundException, IOException {
		File configFile = new File("/home/henkej/.piadev");
		if (configFile.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(configFile));
			String baseUrl = props.getProperty("baseurlKC");
			String username = props.getProperty("usernameKC");
			String password = props.getProperty("passwordKC");
			PiaKeycloakRestService service = new PiaKeycloakRestService();
			String token = service.login(baseUrl, "pia-admin-realm", username, password, "pia-admin-web-app-client");
			assertNotNull(token);
			String pseudonym = props.getProperty("new.username");
			String userPassword = props.getProperty("new.password");
			String eMail = props.getProperty("new.email");
			try {
				service.login(baseUrl, "pia-proband-realm", pseudonym, userPassword, "pia-proband-web-app-client");
				service.addEmail(baseUrl, token, pseudonym, eMail);
			} catch (HttpClientErrorException e) {
				String jsonPart = e.getResponseBodyAsString();
				JsonError errorBean = new Gson().fromJson(jsonPart, JsonError.class);
				if (errorBean == null) {
					throw new IOException(jsonPart, e);
				}
				throw new IOException(errorBean.getMessage());
			}
		} else {
			fail(String.format("%s does not exist...", configFile.getAbsolutePath()));
		}
	}
}

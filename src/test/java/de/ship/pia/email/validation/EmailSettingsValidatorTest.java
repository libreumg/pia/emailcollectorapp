package de.ship.pia.email.validation;


import static org.assertj.core.api.Assertions.assertThat;

import de.ship.pia.TestSupport;
import de.ship.pia.common.ValidationMessage;
import de.ship.pia.email.EmailSettings;
import org.junit.jupiter.api.Test;
import org.springframework.context.MessageSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

class EmailSettingsValidatorTest {

    private Validator validator = TestSupport.validator();
    private MessageSource messageSource = TestSupport.messageSource();

    @Test
    void bothEmailOptionsTrueIsValid() {
        // given
        EmailSettings emailSettings = new EmailSettings();
        emailSettings.setSaveEmailForPia(true);
        emailSettings.setSaveEmailForShip(true);

        // when
        Set<ConstraintViolation<EmailSettings>> errors = validator.validate(emailSettings);

        // then
        assertThat(errors).hasSize(0);
    }

    @Test
    void bothEmailOptionsFalseIsValid() {
        // given
        EmailSettings emailSettings = new EmailSettings();
        emailSettings.setSaveEmailForPia(false);
        emailSettings.setSaveEmailForShip(false);

        // when
        Set<ConstraintViolation<EmailSettings>> errors = validator.validate(emailSettings);

        // then
        assertThat(errors).hasSize(0);
    }

    @Test
    void bothEmailOptionsDifferentIsValid() {
        // given
        EmailSettings emailSettings = new EmailSettings();
        emailSettings.setSaveEmailForPia(true);
        emailSettings.setSaveEmailForShip(false);

        // when
        Set<ConstraintViolation<EmailSettings>> errors = validator.validate(emailSettings);

        // then
        assertThat(errors).hasSize(0);
    }

    @Test
    void oneEmailOptionNullIsInvalid() {
        // given
        EmailSettings emailSettings = new EmailSettings();
        emailSettings.setSaveEmailForPia(true);

        // when
        Set<ConstraintViolation<EmailSettings>> errors = validator.validate(emailSettings);
        List<String> errorMessageKeys = errors.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        List<String> errorMessages = errorMessageKeys.stream()
                .map(msgKey -> messageSource.getMessage(msgKey.replaceAll("[{}]", ""), null, Locale.GERMAN))
                .collect(Collectors.toList());

        // then
        assertThat(errors).hasSize(1);
        assertThat(errorMessageKeys).contains(ValidationMessage.EMAIL_SETTINGS_ERROR_MESSAGE);
        assertThat(errorMessages).containsOnly("Die Auswahl der gewünschten E-Mail-Einstellungen darf nicht leer sein.");
    }

    @Test
    void bothEmailOptionsNullIsInvalid() {
        // given
        EmailSettings emailSettings = new EmailSettings();

        // when
        Set<ConstraintViolation<EmailSettings>> errors = validator.validate(emailSettings);
        List<String> errorMessageKeys = errors.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        List<String> errorMessages = errorMessageKeys.stream()
                .map(msgKey -> messageSource.getMessage(msgKey.replaceAll("[{}]", ""), null, Locale.GERMAN))
                .collect(Collectors.toList());

        // then
        assertThat(errors).hasSize(2);
        assertThat(errorMessageKeys).containsOnly(ValidationMessage.EMAIL_SETTINGS_ERROR_MESSAGE);
        assertThat(errorMessages).containsOnly("Die Auswahl der gewünschten E-Mail-Einstellungen darf nicht leer sein.");
    }

}
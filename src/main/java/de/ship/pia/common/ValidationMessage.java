package de.ship.pia.common;

public class ValidationMessage {

    private ValidationMessage() {};

    public static final String SUCCESS = "successMessage";

    public static final String SUCCESS_MESSAGE = "success.message";

    public static final String ERROR = "errorMessage";

    public static final String ERROR_STRING = "errorString";

    public static final String FORM_ERROR = "error.form";

    public static final String LOGIN_ERROR_MESSAGE = "error.login.failure";

    public static final String EMAIL_SETTINGS_ERROR_MESSAGE = "{error.email.settings}";

    public static final String LOGIN_FAILURE = "error.login.too.many.attempts";

    public static final String LOGIN_WRONG_STUDY = "error.login.wrong.study";

}

package de.ship.pia.email;

import de.ship.pia.email.validation.EmailSettingsValidation;

@EmailSettingsValidation
public class EmailSettings {

    private String id;

    private String password;

    private String email;

    private Boolean saveEmailForPia;

    private Boolean saveEmailForShip;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getSaveEmailForPia() {
        return saveEmailForPia;
    }

    public void setSaveEmailForPia(Boolean saveEmailForPia) {
        this.saveEmailForPia = saveEmailForPia;
    }

    public Boolean getSaveEmailForShip() {
        return saveEmailForShip;
    }

    public void setSaveEmailForShip(Boolean saveEmailForShip) {
        this.saveEmailForShip = saveEmailForShip;
    }
}

package de.ship.pia.email.validation;

import de.ship.pia.email.EmailSettings;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailSettingsValidator implements ConstraintValidator<EmailSettingsValidation, EmailSettings> {

    @Override
    public boolean isValid(EmailSettings emailSettings, ConstraintValidatorContext context) {

        boolean valid = true;

        if (emailSettings.getSaveEmailForPia() == null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                    .addPropertyNode("saveEmailForPia")
                    .addConstraintViolation();
            valid = false;
        }

        if (emailSettings.getSaveEmailForShip() == null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                    .addPropertyNode("saveEmailForShip")
                    .addConstraintViolation();
            valid = false;
        }

        return valid;
    }

}

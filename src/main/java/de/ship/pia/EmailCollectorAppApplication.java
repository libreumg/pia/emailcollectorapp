package de.ship.pia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author henkej
 *
 */
@SpringBootApplication
public class EmailCollectorAppApplication{

	public static void main(String[] args) {
		SpringApplication.run(EmailCollectorAppApplication.class, args);
	}

}

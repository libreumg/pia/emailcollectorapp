package de.ship.pia.index;

/**
 * 
 * @author henkej
 *
 */
public enum PiaErrorMessage {
	/**
	 * if username and password combination fails
	 */
	LOGIN_FAILED("Sorry, Login failed."),
	
	/**
	 * if too many trials have been made; timeout set for (currently 5) minutes
	 */
	FAILED_LOGIN_ATTEMPTS("User has 3 failed login attempts"),
	
	/**
	 * if a user is not a participant, but an administator or such
	 */
	WRONG_STUDY("The given pseudonym could not be found in the studies you have access to."),
	
	/**
	 * if a user is not a participant, but an administrator or such (keycloak version)
	 */
	KC_WRONG_STUDY("Requesting user has no access to study \"${expectedStudyName}\"");

	private final String value;

	private PiaErrorMessage(String value) {
		this.value = value;
	}

	/**
	 * compare the value of this enum to the comparison content
	 * 
	 * @param comparison the comparison
	 * @return true or false
	 */
	public Boolean equals(String comparison) {
		return value.equals(comparison);
	}

	public String get() {
		return this.value;
	}
}

package de.ship.pia.index.impl.piarest;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class PiaVersionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String PIPELINE_ID;
	private String GIT_HASH;
	private String GIT_REF;

	public Boolean isKeycloakAware() {
		return GIT_REF == null ? null
				: (GIT_REF.startsWith("release-1-30-") || GIT_REF.startsWith("release-1-31-")
						|| GIT_REF.startsWith("release-1-32-"));
		// TODO: from version 1-30 on, remove the old rest repository as it is not needed any longer
	}

	/**
	 * @return the pIPELINE_ID
	 */
	public String getPIPELINE_ID() {
		return PIPELINE_ID;
	}

	/**
	 * @param pIPELINE_ID the pIPELINE_ID to set
	 */
	public void setPIPELINE_ID(String pIPELINE_ID) {
		PIPELINE_ID = pIPELINE_ID;
	}

	/**
	 * @return the gIT_HASH
	 */
	public String getGIT_HASH() {
		return GIT_HASH;
	}

	/**
	 * @param gIT_HASH the gIT_HASH to set
	 */
	public void setGIT_HASH(String gIT_HASH) {
		GIT_HASH = gIT_HASH;
	}

	/**
	 * @return the gIT_REF
	 */
	public String getGIT_REF() {
		return GIT_REF;
	}

	/**
	 * @param gIT_REF the gIT_REF to set
	 */
	public void setGIT_REF(String gIT_REF) {
		GIT_REF = gIT_REF;
	}
}

package de.ship.pia.index.impl.piarest;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class Model2KeycloakBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String access_token;
	private Integer expires_in;
	private Integer refresh_expires_in;
	private String refresh_token;
	private String token_type;
	private String session_state;
	private String scope;

	/**
	 * @return the access_token
	 */
	public String getAccess_token() {
		return access_token;
	}

	/**
	 * @return the expires_in
	 */
	public Integer getExpires_in() {
		return expires_in;
	}

	/**
	 * @param expires_in the expires_in to set
	 */
	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}

	/**
	 * @return the refresh_expires_in
	 */
	public Integer getRefresh_expires_in() {
		return refresh_expires_in;
	}

	/**
	 * @param refresh_expires_in the refresh_expires_in to set
	 */
	public void setRefresh_expires_in(Integer refresh_expires_in) {
		this.refresh_expires_in = refresh_expires_in;
	}

	/**
	 * @return the refresh_token
	 */
	public String getRefresh_token() {
		return refresh_token;
	}

	/**
	 * @param refresh_token the refresh_token to set
	 */
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	/**
	 * @return the token_type
	 */
	public String getToken_type() {
		return token_type;
	}

	/**
	 * @param token_type the token_type to set
	 */
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	/**
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * @param scope the scope to set
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * @param access_token the access_token to set
	 */
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	/**
	 * @return the session_state
	 */
	public String getSession_state() {
		return session_state;
	}

	/**
	 * @param session_state the session_state to set
	 */
	public void setSession_state(String session_state) {
		this.session_state = session_state;
	}
}

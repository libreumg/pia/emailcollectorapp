package de.ship.pia.index.impl.piarest;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class JsonError implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer statusCode;
	private String error;
	private String message;

	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}

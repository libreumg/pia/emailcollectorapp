package de.ship.pia.index.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import de.ship.pia.index.impl.piarest.Model2Bean;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class PiaOldRestService implements PiaRestInterface {

	@Override
	public String login(String baseUrl, String realm, String username, String password, String clientId) {
		StringBuilder buf = new StringBuilder(baseUrl);
		buf.append("/api/v1/user/login");
		String uri = buf.toString();

		RestTemplate template = new RestTemplate();

		Map<String, String> params = new HashMap<>();
		params.put("username", username);
		params.put("password", password);
		params.put("locale", "de-DE");
		params.put("logged_in_with", "web");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		String body = new Gson().toJson(params);

		HttpEntity<String> entity = new HttpEntity<>(body, headers);

		Model2Bean bean = template.postForObject(uri, entity, Model2Bean.class, new HashMap<>());
	
		return bean.getToken();
	}

	@Override
	public void addEmail(String baseUrl, String pmToken, String pseudonym, String eMail) {
		StringBuilder buf = new StringBuilder(baseUrl);
		buf.append("/api/v1/personal/personalData/proband/");
		buf.append(pseudonym);
		String uri = buf.toString();

		RestTemplate template = new RestTemplate();

		Map<String, String> params = new HashMap<>();
		params.put("email", eMail);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(pmToken);

		String body = new Gson().toJson(params);

		HttpEntity<String> entity = new HttpEntity<>(body, headers);

		template.put(uri, entity);
	}
}

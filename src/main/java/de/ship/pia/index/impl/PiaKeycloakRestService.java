package de.ship.pia.index.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import de.ship.pia.index.impl.piarest.Model2KeycloakBean;

/**
 * 
 * @author henkej
 *
 */
@Service
public class PiaKeycloakRestService implements PiaRestInterface {

	@Override
	public String login(String baseUrl, String realm, String username, String password, String clientId) {
		StringBuilder buf = new StringBuilder(baseUrl);
		buf.append("/api/v1/auth/realms/").append(realm).append("/protocol/openid-connect/token");
		String uri = buf.toString();

		RestTemplate template = new RestTemplate();

		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("grant_type", "password");
		map.add("client_id", clientId);
		map.add("username", username);
		map.add("password", password);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

		Model2KeycloakBean bean = template.postForObject(uri, entity, Model2KeycloakBean.class, new HashMap<>());

		return bean.getAccess_token();
	}

	@Override
	public void addEmail(String baseUrl, String pmToken, String pseudonym, String eMail) {
		StringBuilder buf = new StringBuilder(baseUrl);
		buf.append("/admin/api/v1/personal/personalData/proband/");
		buf.append(pseudonym);
		String uri = buf.toString();

		RestTemplate template = new RestTemplate();

		Map<String, String> params = new HashMap<>();
		params.put("email", eMail);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(pmToken);

		String body = new Gson().toJson(params);

		HttpEntity<String> entity = new HttpEntity<>(body, headers);

		template.put(uri, entity);
	}
}

package de.ship.pia.index.impl;

import static de.ship.pia.emailcollector.jooq.Tables.T_PERSON;
import static de.ship.pia.emailcollector.jooq.Tables.V_VERSION;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.pia.emailcollector.jooq.tables.records.TPersonRecord;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class DatabaseRepository {
	private static final Logger LOGGER = LogManager.getLogger(DatabaseRepository.class);

	private static final int EXPECTED_VERSION = 1;

	private final DSLContext jooq;

	public DatabaseRepository(@Autowired DSLContext jooq) {
		this.jooq = jooq;
		Integer version = jooq.selectFrom(V_VERSION).fetchOne(V_VERSION.DB_VERSION);
		if (version < EXPECTED_VERSION) {
			String message = String.format("Database version %d is too slow, need at least %d", version, EXPECTED_VERSION);
			throw new DataAccessException(message);
		}
	}

	/**
	 * add the records to the database
	 * 
	 * @param pseudoId the pseudo ID
	 * @param eMail the email
	 * @param consentPia the time of the consent for PIA; null, if not set
	 * @param consentShip the time of the consent for SHIP; null, if not set
	 * @return the number of affected database changes; should be 1
	 */
	public Integer addEmail(String pseudoId, String eMail, LocalDateTime consentPia, LocalDateTime consentShip) {
		InsertOnDuplicateSetMoreStep<TPersonRecord> sql = jooq
		// @formatter:off
			.insertInto(T_PERSON,
					        T_PERSON.PSEUDO_ID,
					        T_PERSON.EMAIL,
					        T_PERSON.REGISTERED,
					        T_PERSON.CONSENT_PIA,
					        T_PERSON.CONSENT_SHIP)
			.values(pseudoId, eMail, true, consentPia, consentShip)
			.onConflict(T_PERSON.PSEUDO_ID)
			.doUpdate()
			.set(T_PERSON.EMAIL, eMail)
			.set(T_PERSON.REGISTERED, true)
			.set(T_PERSON.CONSENT_PIA, consentPia)
			.set(T_PERSON.CONSENT_SHIP, consentShip);
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}
}

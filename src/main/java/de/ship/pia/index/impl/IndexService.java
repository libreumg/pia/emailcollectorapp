package de.ship.pia.index.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;

import de.ship.pia.emailcollector.jooq.tables.records.TPersonRecord;
import de.ship.pia.index.IIndexService;
import de.ship.pia.index.impl.piarest.JsonError;

/**
 * 
 * @author henkej
 *
 */
@Service
public class IndexService implements IIndexService {

	@Autowired
	private DatabaseRepository databaseRepository;

	@Autowired
	private PiaOldRestService piaOldRestService;

	@Autowired
	private PiaKeycloakRestService piaKeycloakRestService;

	@Override
	public String submit(TPersonRecord bean, String userPass, String baseUrl, String pmUsername, String pmPassword) {
		String pseudoId = bean.getPseudoId();
		String eMail = bean.getEmail();
		LocalDateTime consentPia = bean.getConsentPia();
		LocalDateTime consentShip = bean.getConsentShip();
		PiaRestInterface piaInterface = piaKeycloakRestService;
		Boolean isKeycloakAware = piaKeycloakRestService.isKeycloakAware(baseUrl);
		if (isKeycloakAware == null) {
			throw new NullPointerException("could not determine version of PIA from the version page");
		} else if (isKeycloakAware) {
			piaInterface = piaKeycloakRestService;
		} else if (!isKeycloakAware) {
			piaInterface = piaOldRestService;
		}
		try {
			piaInterface.login(baseUrl, "pia-proband-realm", pseudoId, userPass, "pia-proband-web-app-client"); // on invalid logins, throws an Exception
			databaseRepository.addEmail(pseudoId, eMail, consentPia, consentShip); // always rescue all
			String pmToken = piaInterface.login(baseUrl, "pia-admin-realm", pmUsername, pmPassword, "pia-admin-web-app-client");
			piaInterface.addEmail(baseUrl, pmToken, pseudoId, eMail);
			return null;
		} catch (HttpClientErrorException e) {
			String jsonPart = e.getResponseBodyAsString();
			JsonError errorBean = new Gson().fromJson(jsonPart, JsonError.class);
			return errorBean.getMessage();
		}	
	}
}

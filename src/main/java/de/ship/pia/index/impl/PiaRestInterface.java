package de.ship.pia.index.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import de.ship.pia.index.impl.piarest.PiaVersionBean;

/**
 * 
 * @author henkej
 *
 */
public interface PiaRestInterface {

	/**
	 * check if the pia instance under baseUrl is keycloak aware
	 * 
	 * @param baseUrl the base URL of pia
	 * @return true or false
	 */
	public default Boolean isKeycloakAware(String baseUrl) {
		String uri = new StringBuilder(baseUrl).append("/version").toString();

		ResponseEntity<String> response = new RestTemplate().getForEntity(uri, String.class);

		PiaVersionBean bean = new Gson().fromJson(response.getBody(), PiaVersionBean.class);

		return bean.isKeycloakAware();
	}

	/**
	 * login to the PIA instance and return the token
	 * 
	 * @param baseUrl  the PIA base URL
	 * @param realm    the keycloak realm
	 * @param username the username of the Probandenmanager
	 * @param password the password of the Probandenmanager
	 * @param clientId the client ID for the keycloak connection
	 * @return the token
	 */
	public String login(String baseUrl, String realm, String username, String password, String clientId);

	/**
	 * add the email for the pseudonym
	 * 
	 * @param baseUrl   the PIA base URL
	 * @param pmToken   the Probandenmanagment token (get it by calling login method
	 *                  before)
	 * @param pseudonym the pseudonym of the study participant
	 * @param eMail     the email of the user referenced by pseudonym
	 */
	public void addEmail(String baseUrl, String pmToken, String pseudonym, String eMail);
}

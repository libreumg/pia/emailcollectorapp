package de.ship.pia.index;

import de.ship.pia.common.ValidationMessage;
import de.ship.pia.email.EmailSettings;
import de.ship.pia.emailcollector.jooq.tables.records.TPersonRecord;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class IndexController {

	public final static String INDEX_PAGE = "index";
	private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(IndexController.class);

	@Autowired
	private IIndexService indexService;

	@Value("${pia.baseurl:https://pia.ship-med.uni-greifswald.de}")
	private String baseUrl;

	@Value("${pia.pm.username}")
	private String pmUsername;

	@Value("${pia.pm.password}")
	private String pmPassword;

	@GetMapping("/")
	public String homePage(final Model model) {
		model.addAttribute("emailSettings", new EmailSettings());
		return INDEX_PAGE;
	}

	@PostMapping("/")
	public String submitForm(@Valid final EmailSettings emailSettings,
							 final BindingResult bindingResult,
							 final Model model) {

		if (bindingResult.hasErrors()) {
			model.addAttribute(ValidationMessage.ERROR, ValidationMessage.FORM_ERROR);
			return INDEX_PAGE;
		}
		TPersonRecord bean = getPersonRecordBean(emailSettings);
		String result = indexService.submit(bean, emailSettings.getPassword(), baseUrl, pmUsername, pmPassword);
		if (result != null) {
			LOGGER.error(result);
			if (PiaErrorMessage.LOGIN_FAILED.equals(result)) {
				model.addAttribute(ValidationMessage.ERROR, ValidationMessage.LOGIN_ERROR_MESSAGE);
			} else if (PiaErrorMessage.FAILED_LOGIN_ATTEMPTS.equals(result)) {
				model.addAttribute(ValidationMessage.ERROR, ValidationMessage.LOGIN_FAILURE);
			} else if (PiaErrorMessage.WRONG_STUDY.equals(result)) {
				model.addAttribute(ValidationMessage.ERROR, ValidationMessage.LOGIN_WRONG_STUDY);
			} else {
				model.addAttribute(ValidationMessage.ERROR_STRING, result);
			}
		} else {
			model.addAttribute(ValidationMessage.SUCCESS, ValidationMessage.SUCCESS_MESSAGE);
		}

		return INDEX_PAGE;
	}

	private TPersonRecord getPersonRecordBean(final EmailSettings emailSettings) {
		TPersonRecord bean = new TPersonRecord();
		bean.setConsentPia(emailSettings.getSaveEmailForPia() ? LocalDateTime.now() : null);
		bean.setConsentShip(emailSettings.getSaveEmailForShip() ? LocalDateTime.now() : null);
		bean.setEmail(emailSettings.getEmail());
		bean.setPseudoId(emailSettings.getId());
		return bean;
	}
}

package de.ship.pia.index;

import de.ship.pia.emailcollector.jooq.tables.records.TPersonRecord;

/**
 * 
 * @author henkej
 *
 */
public interface IIndexService {
	/**
	 * submit the form request
	 * 
	 * @param bean       the form field contents in a bean
	 * @param userPass   the password of the user
	 * @param baseUrl    the base URL of the PIA instance
	 * @param pmUsername the user name of the Probandenmanager account
	 * @param pmPassword the password of the Probandenmanager account
	 * @return the error if any; if null, the submit method was successful
	 */
	public String submit(TPersonRecord bean, String userPass, String baseUrl, String pmUsername, String pmPassword);
}
